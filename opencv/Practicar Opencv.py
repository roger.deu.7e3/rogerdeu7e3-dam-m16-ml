import transformations as tf
import cv2 as cv

didac = cv.imread("fotos/didac.png")
roger = cv.imread("fotos/roger.jpg")


alpha = 0.5
didacresized = tf.resizewithsize(didac,newwidth=roger.shape[1], newheight=roger.shape[0])

superposicio = cv.addWeighted(roger, alpha, didacresized, 1-alpha, 0)

#55,13 235,13 | 55,270

cropped = roger[141:158, 90:190]
cropped_h, cropped_w = cropped.shape[:2]

# cv.imshow("Cropped", cropped)
# roger[90:90]
roger[90:90+cropped_h, 90:90+cropped_w] = cropped


cv.imshow("Foto", roger)
cv.waitKey(0)
