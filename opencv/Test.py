import cv2 as cv
from transformations import *

def resize(src, scale=0.5, interpolation = cv.INTER_AREA):
    width = src.shape[1]
    height = src.shape[0]
    dimensio = (int(width*scale), int(height*scale))
    return cv.resize(src, dimensio, interpolation)


img = cv.imread("itb.jpg");
width = img.shape[0]
height = img.shape[1]

print("Amplada: ",width)
print("Altura: ",height)

resized = resize(img)
cv.imshow("ITB", resized)

resize2 = cv.resize(img,(200,200),cv.INTER_AREA)

# cv.imshow("ITB", resize2)
# cv.imshow("ITB", img)

transalted = translate(img, 50, 100)
cv.imshow("Josep Maria Bartomeu i Floreta ", transalted)

rotated = rotate(img, 60, (100,100))
cv.imshow("Rotated", rotated)

flipped = flip(img,1)
cv.imshow("Bones", flipped)

cropped = crop(img, (120,100), (500,500))
cv.imshow("Cropped", cropped)

cv.waitKey(0)
