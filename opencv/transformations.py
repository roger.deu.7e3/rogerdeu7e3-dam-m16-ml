import cv2 as cv
import numpy as np

def resize(src, scale=0.5, interpolation = cv.INTER_AREA):
    width = src.shape[1]
    height = src.shape[0]
    dimensio = (int(width*scale), int(height*scale))
    return cv.resize(src, dimensio, interpolation)

def resizewithsize(src, newwidth, newheight, interpolation = cv.INTER_AREA):
    width = src.shape[1]
    height = src.shape[0]
    dimensio = (int(newwidth), int(newheight))
    return cv.resize(src, dimensio, interpolation)

def translate(img, x, y):
    transMat = np.float32([[1, 0, x], [0, 1, y]])
    dimensions = (img.shape[1], img.shape[0])
    return cv.warpAffine(img, transMat, dimensions)

def rotate(img, angle, rotPoint=None):
    (height,width) = img.shape[:2]

    if rotPoint is None:
        rotPoint = (width//2,height//2)

    rotMat = cv.getRotationMatrix2D(rotPoint, angle, 1.0)
    dimensions = (width,height)

    return cv.warpAffine(img, rotMat, dimensions)

def flip(img, numero):
    return cv.flip(img, numero)

def crop(img, firstpixel, secondpixel):
    return img[200:400, 300:400]
