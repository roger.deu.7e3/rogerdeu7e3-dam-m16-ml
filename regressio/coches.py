import inline
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import scipy.stats as stats
from sklearn import linear_model
matplotlib.style.use('ggplot')

data = pd.read_csv("../datasets/data/mtcars.csv")

data.plot(kind="scatter",
           x="wt",
           y="mpg",
           figsize=(9,9),
           color="black");
plt.show()
# Initialize model
regression_model = linear_model.LinearRegression()

# Train the model using the data data
regression_model.fit(X = pd.DataFrame(data["wt"]),
                     y = data["mpg"])

# Check trained model y-intercept
print(regression_model.intercept_)

# Check trained model coefficients
print(regression_model.coef_)

print(regression_model.score(X = pd.DataFrame(data["wt"]), y= data["mpg"]))

train_prediction = regression_model.predict(X = pd.DataFrame(data["wt"]))

# Actual - prediction = residuals
residuals = data["mpg"] - train_prediction

print(residuals.describe())


SSResiduals = (residuals**2).sum()

SSTotal = ((data["mpg"] - data["mpg"].mean())**2).sum()

# R-squared
1 - (SSResiduals/SSTotal)


data.plot(kind="scatter",
           x="wt",
           y="mpg",
           figsize=(9,9),
           color="black",
           xlim = (0,7))

# Plot regression line
plt.plot(data["wt"],      # Explanitory variable
         train_prediction,  # Predicted values
         color="blue");
plt.show()
