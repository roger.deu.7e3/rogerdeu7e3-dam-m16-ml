files = int(input("Introdueix el numero de files a mostrar: "))
import math


def combination(n, r):
    return int((math.factorial(n)) / ((math.factorial(r)) * math.factorial(n - r)))


def for_test(x, y):
    for y in range(x):
        return combination(x, y)


def tirangle_pascal(files):
    result = []

    for i in range(files):

        files = []
        for element in range(i + 1):
            files.append(combination(i, element))
        result.append(files)

    return result


for files in tirangle_pascal(files):
    print(files)
