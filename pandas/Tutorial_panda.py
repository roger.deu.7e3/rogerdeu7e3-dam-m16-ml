import pandas as pd
import matplotlib.pyplot as plt

titanic = pd.read_csv("../datasets/data/titanic.csv")
print(titanic.head())

#Exericic 1
print(titanic.shape)
#Exercici 2
print(titanic.columns)
#Exercici 3
print(titanic.head())
#Exercici 4
print(titanic[["Age","Sex","Name"]].head())
#Exercici 5
menors30=titanic[titanic["Age"] < 30]
print(titanic[titanic["Age"] < 30])
#Exercici 6
print(menors30[["Age","Sex","Name"]])
#Exercici 7
print(titanic["Age"].min())
#Exercici 8
print(titanic.loc[titanic["Age"].max(), "Name"])
#Exercici 9
supervivents=titanic[titanic["Survived"]==1]
print(supervivents["Age"].mean)
#Exercici 10
hist=titanic["Age"]
plt.hist(hist,bins=10)
plt.show()

data_female =titanic.loc[titanic["Sex"]=="female","Age"]
data_male =titanic.loc[titanic["Sex"]=="male","Age"]
plt.hist()
