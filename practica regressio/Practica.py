import re
import inline
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import scipy.stats as stats
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures

data = pd.read_csv("../datasets/data/laptops.csv", encoding="latin-1")


def extract_Ram(x):
    match = re.search(r"(\d+)[GT]B", x)
    if match:
        return int(match.groups()[0])
    else:
        return x


def extract_Cpu(x):
    # match = re.search(r"(\d+(?:\.\d+)?)(GHz)",x)
    match = re.search(r"(\d+\.?\d*?)GHz", x)
    if match:
        return float(match.groups()[0])
    else:
        raise Exception("Error al treure la CPU", x)


def extract_Res(x):
    match = re.search(r"(\d+)(x\d+)", x)
    if match:
        return float(match.groups()[0])
        # print(float(match.groups()[0]))
    else:
        raise Exception("Error al treure la resolucio", x)


def extract_Pes(x):
    match = re.search(r"(\d+\.?\d*?)kg", x)
    if match:
        return float(match.groups()[0])
    else:
        raise Exception("Error al treure el pes", x)


def train(vector_caracteristiques, plot=False):
    x = data[vector_caracteristiques]
    y = data.loc[:, ["Price_euros"]]
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.20, random_state=2020, shuffle=True)
    lr = LinearRegression()
    lr.fit(X_train, y_train)
    if plot:
        plt.scatter(X_test, y_test, color="pink")
        plt.plot(X_test, lr.predict(X_test), color="yellow")
        plt.show()
    return lr, lr.score(X_test, y_test)


data["ScreenResolution"] = data["ScreenResolution"].apply(extract_Res)
data["Cpu"] = data["Cpu"].apply(extract_Cpu)
data["Ram"] = data["Ram"].apply(extract_Ram)
data["Weight"] = data["Weight"].apply(extract_Pes)

model, score = train(["Cpu", "Ram"], False)
print("Cpu y Ram ",score)

model, score = train(["Cpu", "Ram", "ScreenResolution"], False)
print("Cpu, ram i resolution ",score)

model, score = train(["ScreenResolution", "Ram", "Cpu", "Weight"], False)
print("Cpu, ram, resolution and weight ",score)

model, score = train(["ScreenResolution"], True)
print("Screen resolution ",score)

model, score = train(["Weight"], True)
print("Weight ",score)

model, score = train(["Cpu"], True)
print("Cpu ",score)

model, score = train(["Ram"], True)
print("Ram ",score)

def trainPoly(vector_caracteristiques,degree):
    x = data[vector_caracteristiques]
    y = data.loc[:, ["Price_euros"]]
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.20, random_state=2020, shuffle=True)
    lr = make_pipeline(PolynomialFeatures(degree), LinearRegression())
    lr.fit(X_train, y_train)
    return lr.score(X_test, y_test)

print("Cpu y Ram polinomicas: ", trainPoly(["Cpu", "Ram"],3))

print("Cpu, Ram y resolucion polinomicas: ", trainPoly(["Cpu", "Ram", "ScreenResolution"],2))

print("Cpu, Ram, Resolucion i peso polinomicas: ", trainPoly(["Cpu", "Ram", "ScreenResolution","Weight"],2))

for i in [1,2,3,4,5]:
    print("Cpu y Ram polinomicas de grado ",i, trainPoly(["Cpu", "Ram"],i))
    print("Cpu, Ram y resolucion polinomicas de grado ", i, trainPoly(["Cpu", "Ram", "ScreenResolution"], i))
    print("Cpu, Ram, Resolucion y peso polinomicas de grado ", i, trainPoly(["Cpu", "Ram", "ScreenResolution", "Weight"], i))
