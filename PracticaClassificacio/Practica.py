import pandas as pd
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.naive_bayes import GaussianNB
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import numpy as np
from sklearn.datasets import load_iris

iris = pd.read_csv('../datasets/data/iris.csv')

names = ["sepal_length","sepal_width","petal_length","petal_width"]


def trainModel(X, opcio, numero):
    global model
    y = iris["species"]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=2020)

    if opcio == 0:
        model = GaussianProcessClassifier()
    elif opcio == 1:
        model = KNeighborsClassifier(numero)
    elif opcio == 2:
        model = GaussianNB()

    scaler = MinMaxScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    model.fit(X_train, y_train)
    # plt.scatter(X_train, y_train, color="b",label="fast")
    # plt.scatter(X_test, y_test, color="r",label="slow")
    # plt.show()
    return model.score(X_test, y_test)

def trainNeighbours(numeroNeighbors, features, opcio):
    iris = load_iris()

    X = iris.data[:, features]
    y = iris.target
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=2020)
    h = .02
    if (opcio == 1):
        model = KNeighborsClassifier(n_neighbors=numeroNeighbors, weights='uniform')
    elif (opcio == 2):
        model = GaussianProcessClassifier()
    else:
        model = GaussianNB()
    model.fit(X_train, y_train)

    cmap_light = ListedColormap(['orange','cyan','cornflowerblue'])
    cmap_bold = ListedColormap(['darkorange', 'c', 'darkblue'])
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))


    Z = model.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    plt.figure()
    plt.pcolormesh(xx, yy, Z, cmap=cmap_light, shading='auto')
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=cmap_bold, edgecolor='k', s=20)
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    valorx = 'x value', names[features[0]], "y", names[features[1]]
    plt.xlabel(valorx)
    plt.title("KN")
    plt.ylabel(names[features[1]])
    plt.show()

X = iris.loc[:, ["sepal_length"]]
print("Gaussian sepal_length -> ", trainModel(X, 0, 0))

for i in [5,6,7,8]:
    print("Neighbours sepal_length -> ", trainModel(X, 1, i), "la i es: ", i)
    for j in [0,1,2]:
        trainNeighbours(i,[j,j+1],1)



print("Naive Bayes sepal_length -> ", trainModel(X, 2, 0))
trainNeighbours(None, X, 3)

X = iris.loc[:, ["sepal_length","sepal_width"]]
print("Gaussian sepal_length and with -> ", trainModel(X, 0, 0))
trainNeighbours(None, X, 2)
print("Neighbours sepal_length and with -> ", trainModel(X, 1, 2))
print("Naive Bayes sepal_length and with -> ", trainModel(X, 2, 0))
trainNeighbours(None, X, 3)

X = iris.loc[:, ["sepal_length","sepal_width","petal_width"]]
print("Gaussian sepal_length, width and petal width -> ", trainModel(X, 0, 0))
trainNeighbours(None, X, 2)
print("Neighbours sepal_length, width and petal width -> ", trainModel(X, 1, 2))
print("Naive Bayes sepal_length, width and petal width -> ", trainModel(X, 2, 0))
trainNeighbours(None, X, 3)

X = iris.loc[:, ["sepal_length","sepal_width","petal_width","petal_length"]]
print("Gaussian sepal_length, width and petal_length and width -> ", trainModel(X, 0, 0))
trainNeighbours(None, X, 2)
print("Neighbours sepal_length, width and petal_length and width -> ", trainModel(X, 1, 2))
print("Naive Bayes sepal_length, width and petal_length and width -> ", trainModel(X, 2, 0))
trainNeighbours(None, X, 3)



g = sns.pairplot(iris, hue = 'species', markers='.')
plt.show()

