import pandas as pd
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.naive_bayes import GaussianNB
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import numpy as np
from sklearn.datasets import load_iris

iris = pd.read_csv('../datasets/data/iris.csv')

names = ["sepal_length","sepal_width","petal_length","petal_width"]

def modifyY(x):
    if x == 'setosa':
        return 1;
    elif x == 'versicolor':
        return 2;
    else:
        return 3

def trainModel(X, opcio, numero):
    global model
    y = iris["species"]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=2020)

    if opcio == 0:
        model = GaussianProcessClassifier()
    elif opcio == 1:
        model = KNeighborsClassifier(numero)
    elif opcio == 2:
        model = GaussianNB()

    scaler = MinMaxScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    model.fit(X_train, y_train)
    # plt.scatter(X_train, y_train, color="b",label="fast")
    # plt.scatter(X_test, y_test, color="r",label="slow")
    # plt.show()
    return model.score(X_test, y_test)

iris['species'] = iris['species'].apply(modifyY)

def plotModel(features, model):

    X = iris.loc[:, features]
    y = iris['species']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=2020)
    h = .02

    model.fit(X_train, y_train)

    cmap_light = ListedColormap(['orange','cyan','cornflowerblue'])
    cmap_bold = ListedColormap(['darkorange', 'c', 'darkblue'])
    x_min, x_max = X.iloc[:, 0].min() - 1, X.iloc[:, 0].max() + 1
    y_min, y_max = X.iloc[:, 1].min() - 1, X.iloc[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))


    Z = model.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    plt.figure()
    plt.pcolormesh(xx, yy, Z, cmap=cmap_light, shading='auto')
    plt.scatter(X.iloc[:, 0], X.iloc[:, 1], c=y, cmap=cmap_bold, edgecolor='k', s=20)
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    valorx = 'x value', features[0]
    plt.xlabel(valorx)
    plt.title(model)
    valory ='y value', features[1]
    plt.ylabel(valory)
    plt.show()

X = iris.loc[:, ["sepal_length"]]
print("Gaussian sepal_length -> ", trainModel(X, 0, 0))



print("Naive Bayes sepal_length -> ", trainModel(X, 2, 0))

X = iris.loc[:, ["sepal_length","sepal_width"]]
print("Gaussian sepal_length and with -> ", trainModel(X, 0, 0))
print("Neighbours sepal_length and with -> ", trainModel(X, 1, 2))
print("Naive Bayes sepal_length and with -> ", trainModel(X, 2, 0))

X = iris.loc[:, ["sepal_length","sepal_width","petal_width"]]
print("Gaussian sepal_length, width and petal width -> ", trainModel(X, 0, 0))
print("Neighbours sepal_length, width and petal width -> ", trainModel(X, 1, 2))
print("Naive Bayes sepal_length, width and petal width -> ", trainModel(X, 2, 0))

X = iris.loc[:, ["sepal_length","sepal_width","petal_width","petal_length"]]
print("Gaussian sepal_length, width and petal_length and width -> ", trainModel(X, 0, 0))
print("Neighbours sepal_length, width and petal_length and width -> ", trainModel(X, 1, 2))
print("Naive Bayes sepal_length, width and petal_length and width -> ", trainModel(X, 2, 0))



g = sns.pairplot(iris, hue = 'species', markers='.')
plt.show()






for i in [5,6,7,8]:
    print("Neighbours sepal_length -> ", trainModel(X, 1, i), "la i es: ", i)
    for j in [0,1,2]:
        if j == 0:
            plotModel(["sepal_length","sepal_width"], KNeighborsClassifier(i))
        elif j == 1:
            plotModel(["sepal_length","petal_length"], KNeighborsClassifier(i))
        else:
            plotModel(["sepal_length","petal_width"], KNeighborsClassifier(i))

plotModel(["sepal_length","petal_length"], GaussianNB())
plotModel(["sepal_length","petal_length"], KNeighborsClassifier())
plotModel(["sepal_length","petal_length"], GaussianProcessClassifier())
plotModel(["sepal_length","petal_width"], GaussianNB())
plotModel(["sepal_length","petal_width"], KNeighborsClassifier())
plotModel(["sepal_length","petal_width"], GaussianProcessClassifier())
plotModel(["sepal_length","sepal_width"], GaussianNB())
plotModel(["sepal_length","sepal_width"], KNeighborsClassifier())
plotModel(["sepal_length","sepal_width"], GaussianProcessClassifier())
