import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np

mnist = tf.keras.datasets.mnist

(x_train, y_train), (X_test, y_test) = mnist.load_data()

x_train = tf.keras.utils.normalize(x_train, axis=1)
X_test = tf.keras.utils.normalize(X_test, axis=1)

model = tf.keras.models.Sequential()
model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax))

model.compile(optimizer='adam',loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(x_train, y_train, epochs=3)

val_less, val_acc = model.evaluate(X_test, y_test)
print(val_less, val_acc)

model.save('epic_num_reader.model')
new_model = tf.keras.models.load_model('epic_num_reader.model')

prediction = model.predict([X_test])
print("Classe predida", np.argmax(prediction[10]))
print("Classe real", y_test[10])

# plt.imshow(x_train[0], cmap=plt.cm.binary)
# plt.show()
