import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt
import re
import tensorflow_addons as tfa
import numpy as np


def plot_loss(history):
    plt.plot(history.history['loss'], label='loss')
    if history.history.get('val_loss'):
        plt.plot(history.history['val_loss'], label='val_loss')
    plt.xlabel('Epoch')
    plt.yscale('log')
    plt.ylabel('Error')
    plt.legend()
    plt.grid(True)
    plt.show()


def extract_ram(x):
    match = re.search(r"(\d+)[GT]B", x)
    if match:
        return int(match.groups()[0])
    else:
        return x


def extract_cpu(x):
    # match = re.search(r"(\d+(?:\.\d+)?)(GHz)",x)
    match = re.search(r"(\d+\.?\d*?)GHz", x)
    if match:
        return float(match.groups()[0])
    else:
        raise Exception("Error al treure la CPU", x)


def extract_res(x):
    match = re.search(r"(\d+)(x\d+)", x)
    if match:
        return float(match.groups()[0])
        # print(float(match.groups()[0]))
    else:
        raise Exception("Error al treure la resolucio", x)


def extract_pes(x):
    match = re.search(r"(\d+\.?\d*?)kg", x)
    if match:
        return float(match.groups()[0])
    else:
        raise Exception("Error al treure el pes", x)


data = pd.read_csv("../datasets/data/laptops.csv", encoding="latin-1")

data["ScreenResolution"] = data["ScreenResolution"].apply(extract_res)
data["Cpu"] = data["Cpu"].apply(extract_cpu)
data["Ram"] = data["Ram"].apply(extract_ram)
data["Weight"] = data["Weight"].apply(extract_pes)
data["Company"] = pd.Categorical(data["Company"])
data["Company"] = data["Company"].cat.codes
data["TypeName"] = pd.Categorical(data["TypeName"])
data["TypeName"] = data["TypeName"].cat.codes
data["OpSys"] = pd.Categorical(data["OpSys"])
data["OpSys"] = data["OpSys"].cat.codes

train = data.sample(frac=0.8, random_state=19)
test = data.drop(train.index)

features = ["Cpu", "Ram", "Weight", "Company", "TypeName", "OpSys"]
yfeatures = 'Price_euros'

X_train = train.loc[:, features]
y_train = train[yfeatures]

X_test = test.loc[:, features]
y_test = test[yfeatures]

normalizer = tf.keras.layers.experimental.preprocessing.Normalization()
normalizer.adapt(np.array(X_train))

model = tf.keras.models.Sequential()
model.add(normalizer)
model.add(tf.keras.layers.Dense(512, activation=tf.nn.relu))
model.add(tf.keras.layers.Dropout(0.2))
model.add(tf.keras.layers.Dense(512, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(1))

model.compile(optimizer=tf.optimizers.Adam(learning_rate=1e-5), loss="mean_squared_error",
              metrics=tfa.metrics.RSquare(y_shape=(1,)))

historial = model.fit(X_train, y_train, epochs=5000, validation_split=0.2)

val_less, val_acc = model.evaluate(X_test, y_test)
print("Test", val_less, val_acc)

val_less, val_acc = model.evaluate(X_train, y_train)
print("Train", val_less, val_acc)

plot_loss(historial)

