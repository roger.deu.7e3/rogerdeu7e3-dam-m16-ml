import pandas as pd
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt


def modifyY(x):
    if x == 'setosa':
        return 0
    elif x == 'versicolor':
        return 1
    else:
        return 2


def plot_loss(history):
    plt.plot(history.history['loss'], label='loss')
    if history.history.get('val_loss'):
        plt.plot(history.history['val_loss'], label='val_loss')
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.legend()
    plt.grid(True)
    plt.show()


iris = pd.read_csv('../datasets/data/iris.csv')
iris['species'] = iris['species'].apply(modifyY)

train = iris.sample(frac=0.8, random_state=19)
test = iris.drop(train.index)

features = ["sepal_length", "sepal_width", "petal_length", "petal_width"]
yfeatures = 'species'

X_train = train.loc[:, features]
y_train = train[yfeatures]

X_test = test.loc[:, features]
y_test = test[yfeatures]

normalizer = tf.keras.layers.experimental.preprocessing.Normalization()
normalizer.adapt(np.array(X_train))

model = tf.keras.models.Sequential()
model.add(normalizer)
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
model.add(tf.keras.layers.Dropout(0.2))
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
model.add(tf.keras.layers.Dropout(0.2))
model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
model.add(tf.keras.layers.Dense(3, activation=tf.nn.softmax))
model.compile(optimizer=tf.optimizers.Adam(learning_rate=1e-5), loss="sparse_categorical_crossentropy",
              metrics=['accuracy'])

historial = model.fit(X_train, y_train, epochs=3500, validation_split=0.2)

val_less, val_acc = model.evaluate(X_test, y_test)
print("Test", val_less, val_acc)

val_less, val_acc = model.evaluate(X_train, y_train)
print("Train", val_less, val_acc)

prediction = model.predict([X_test])
print("Classe predida", np.argmax(prediction[17]))
print("Classe real: ", y_test.iloc[17])

plot_loss(historial)
