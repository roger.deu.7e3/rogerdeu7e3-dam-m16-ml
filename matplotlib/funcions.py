import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-2, 2, 10)
x2 = np.linspace(-2, 2, 9)
plt.plot(x, x, "-", label='Linial')  
plt.plot(x2, x2**2, "o", label='Quàdratica')
plt.plot(x, x**3, ".", label='Cúbica')
# Imprimim els punts
plt.xlabel('Valors de X')  # Add an x-label to the axes.
plt.ylabel('Valors de Y')  # Add a y-label to the axes.
plt.title("Funcions")  # Add a title to the axes.
plt.legend()  # Add a legend.
plt.show()