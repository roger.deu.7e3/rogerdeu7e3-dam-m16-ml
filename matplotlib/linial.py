import matplotlib.pyplot as plt
import numpy as np
x = np.linspace(-2, 2, 10)
y = x # Funció linial
plt.plot(x, y, "-", label='linear')  # Imprimim els punts
plt.xlabel('x label')  # Add an x-label to the axes.
plt.ylabel('y label')  # Add a y-label to the axes.
plt.title("Simple Plot")  # Add a title to the axes.
plt.legend()  # Add a legend.
plt.show()