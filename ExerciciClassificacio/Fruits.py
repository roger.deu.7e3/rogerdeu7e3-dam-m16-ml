import inline
import matplotlib
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
import matplotlib.patches as mpatches
import matplotlib.cm as cm
from matplotlib.colors import ListedColormap, BoundaryNorm


fruits = pd.read_table('../datasets/data/fruits.csv')

# print(fruits.head())
#
# print(fruits["fruit_name"].unique())
#
# print(fruits.groupby('fruit_name').size())
#
# sns.countplot(fruits["fruit_name"],label="Count")
# plt.show()

def trainDiscriminant(X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=20, random_state=2020)
    scaler = MinMaxScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    logreg = LinearDiscriminantAnalysis()
    logreg.fit(X_train, y_train)
    print(logreg.score(X_test, y_test))

def trainGaussian(X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=20, random_state=2020)
    scaler = MinMaxScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    logreg = GaussianNB()
    logreg.fit(X_train, y_train)
    print(logreg.score(X_test, y_test))

def trainVector(X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=20, random_state=2020)
    scaler = MinMaxScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    logreg = GaussianNB()
    logreg.fit(X_train, y_train)
    print(logreg.score(X_test, y_test))

def trainLogistic(X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=20, random_state=2020)
    scaler = MinMaxScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    logreg = LogisticRegression()
    logreg.fit(X_train, y_train)
    print(logreg.score(X_test, y_test))

def trainTree(X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=20, random_state=2020)
    scaler = MinMaxScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    logreg = DecisionTreeClassifier()
    logreg.fit(X_train, y_train)
    print(logreg.score(X_test, y_test))

def trainNeighbors(X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=20, random_state=2020)
    scaler = MinMaxScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    logreg = KNeighborsClassifier()
    logreg.fit(X_train, y_train)
    print(logreg.score(X_test, y_test))

y = fruits["fruit_name"]
X = fruits.loc[:, ["mass", "width","height","color_score"]]

print("Logistic"), trainLogistic(X, y)

print("Tree"), trainTree(X, y)

print("Neighbors"), trainNeighbors(X, y)

print("Vector"), trainVector(X, y)

print("Discriminant"), trainDiscriminant(X, y)

print("Gaussian"), trainGaussian(X, y)


